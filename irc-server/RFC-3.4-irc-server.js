exports.time = function (args, socket, global) {
	socket.write(global.hms()+'\n');
}

exports.motd = function (args, socket, global) {
	socket.write(global.motd+'\n');
}

exports.lusers = function (args, socket, global) {
	socket.write('Quantidades de:\n');
	var nClients = 0;
	global.clients.forEach(function(client){
		nClients++;
	});
	socket.write('Usuários:'+nClients+'\n');
	var numberOfChannels = 0;
	for(channel in global.channels){
		numberOfChannels++;
	}
	socket.write('Canais:'+numberOfChannels+'\n');
}

exports.version = function (args, socket, global){
	socket.write(global.version);
}

exports.stats = function (args, socket, global){
	switch (args[1]){
		case 'l':
			socket.write('Esta aplicação é Single Server (não há outros servidores conectados à esta rede)\n');
		break;
		case 'm':
			socket.write('Quantas vezes cada comando foi utilizado:\n');
			for(property in global.counter){
				socket.write(property+' : '+global.counter[property]+'\n');
			}
		break;
		case 'o':
			socket.write('Lista de operadores\n');
			global.clients.forEach(function(client){
				if(client.mode.contains('o')){
					socket.write(client.name+'\n');
					socket.write(client.username+'\n');
					socket.write(client.nick+'\n');
				}
			});
		break;
		default:
			socket.write('ERRO: pedido não reconhecido\n');
	}
}

