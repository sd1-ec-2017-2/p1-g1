exports.join = function (args, socket, global) {
//Modelo exemplo
	args.shift(); // get rid of 'join' 
 	if( args.length == 0 || args.length > 2){ // check if user has passed the appropriate amount of arguments
		socket.write("ERRO: Número de argumentos inválido.\n");
	}
		if( args[0] == '0'){ // user wants to leave all channels
		for( name in global.channels ){
				obj = global.channels [ name ] ;
				if( obj.removeUser( socket.nick ) == true){
					socket.write("Você foi removido do canal \""+name+" com sucesso.\n");
					obj.broadcast("["+name+"] o usuario "+socket.nick+" saiu do canal.\n",socket);
				}
		}
		return ;
	}
	
	canais = [];
	keys = [];
	
		if( args[0].indexOf(',') == -1)
			canais.push(args[0]);
		else
			canais = canais.concat( args[0].split(',') );
	if( args.length == 2){ // in case keys are provided
		if( args[1].indexOf(',') == -1)
			keys.push(args[1]);
		else
			keys = keys.concat( args[1].split(',') );
	}
	canais.forEach(function ( cnome ){ // chech whether the channel names are valid or not
		c = new Channel();
		if( c.isValidName( cnome ) == false){
			socket.write("ERRO: Canal com nome inválido: "+cnome+"\n");
			return ;
		}	
	});
	
/////////////////////////////////////////////	
/////////////////////////////////////////////
	i = 0;
	canais.forEach(function (cnome) {
		senha = "";
		if( keys[i]) 
			senha = keys[i];
		if(!global.channels[ cnome ]){ // channel doesn't exist
			canal = new Channel();
			canal.setName( cnome );
			canal.addUser( socket, 'O' ); //
			global.channels[cnome] = canal;
			socket.write("Canal \""+cnome+"\" foi criado com sucesso.\n");
		}
		else { // channel exists
			obj = global.channels[ cnome ];
			if( obj.inviteOnly == true && obj.isInvited( socket.nick ) == false )
				socket.write("ERRO: Canal \""+cnome+"\" é INVITE_ONLY e você não está na lista de convidados.\n");
							
			else {
				if( senha != obj.key){
					socket.write("ERRO: A senha do canal \""+cnome+"\" está incorreta.\n"); 
				}
				else{
					if(obj.isUser( socket.nick ) == true) { socket.write("Você já está no canal \""+cnome+"\".\n"); 
}
					else{ obj.addUser( socket, ""); socket.write("Adicionado ao canal \""+cnome+"\" com sucesso.\n"); 
						obj.broadcast("["+cnome+"] O usuario "+socket.nick+" foi adicinado ao canal.\n",socket);

						} 
					
				}
				
			}
		}
		i++;
	});
	

	
	
};
exports.part = function(args,socket,global){
		args.shift(); // "PART" goes away
		if( args.length == 0) {
				socket.write("ERRO: Número incorreto de Parâmetro.\n");
				return ;
		}
		if(args[1]){
			if(args[1][0] != ":"){
			socket.write("ERRO: Parâmetros inválidos.\n");
			return;
			}
		}
		comment = "";
		canais = [];
		if( args.length == 1)
			comment = "";
		else{
			temp = args;
			temp.shift(); // get rid of the first argumentos
			msg = temp.join("");
			msg = msg.replace(":","");
			comment = "Comentário do usuário: "+msg+"\n";;
		}
		if( args[0].indexOf(',') != -1 )
			canais = canais.concat( args[0].split(',') );
			
		else 
			canais.push(args[0]);
		canais.forEach( function (canal) {
			if(!global.channels[canal])
					socket.write("Canal \""+canal+"\" não foi encontrado.\n");
			
			else{
				ret = global.channels[canal].removeUser( socket.nick );
				if( ret == true){
					socket.write("Você saiu do canal \""+canal+"\" com sucesso.\n");
					global.channels[canal].broadcast("["+canal+"] O usuário "+socket.nick+" saiu do canal.\n"+comment,socket);
				}
				else
					socket.write("Voce não está no canal \""+canal+"\".\n");
				
			}
			
		});
		
			
};


exports.topic = function(args,socket,global){
	args.shift();

	if( args.length == 0){
		socket.write("ERRO: Numero de parametros invalidos.\n");
		return;
	}

	canal = args[0];
	if(!global.channels[canal]){
				socket.write("Canal não encontrado.\n");
				return;
		}
		obj = global.channels[canal];
	if( args.length == 1){		
		if( obj.topic == "")
			socket.write("O canal não tem topico definido.\n");
		else
			socket.write("O topico do canal é: "+obj.topic+"\n.");
	}
	if( args.length >= 2){
		args.shift();
		temp = args.join(" ");
		topic = temp.replace(":","");
		obj.topic = topic;
		socket.write("Tópico alterado com sucesso.\n");
		obj.broadcast("["+canal+"] topico alterado para: "+topic+".\n",socket );
	}
	
	};

/*
////////////////////////////////////////////////////////
////////////////// Funções Auxiliares //////////////////
////////////////////////////////////////////////////////
*/
function Channel(){
		this.name = "";
		this.opsTopic = false;
		this.inviteList = []; 
		this.inviteOnly = false;
		this.topic = "";
		this.limit = 0; // 
		this.channelModes = [];
	//	this.clients = new Array();
		this.users = {};
		this.key = "";
		
	this.isValidName = function (name){
		if( name.length > 50) return false; 
		if( name[0] == '#') return true;
		if( name[0] == '&') return true;
		if( name[0] == '!') return true;
		if( name[0] == '+') return true;
		return false;
	};
	this.removeChannelMode = function (modo){
		rt = this.channelModes.indexOf(modo);
		if( rt == -1)
			return false;
		this.channelModes.splice(rt,1);
		return true;
	};
	this.setChannelMode = function ( modo ){
		rt = this.channelModes.indexOf(modo);
		if( rt != -1)
			return false;
		
		this.channelModes.push( modo );
		return true;
	};
	this.getName = function (){
		return this.name;
	};
	this.getMode = function (name){
		return this.users[ name ].mode;
	}
	this.setName = function (nome){
		this.name = nome;
	};
	this.addUser = function(socket, mode){
		
		global = Array.prototype.GLOBAL;
		if(!global.USERS[socket.nick]){
			global.USERS[socket.nick] = {};
			global.USERS[socket.nick]['channels'] = new Array();
		}	
		
		global.USERS[socket.nick].channels.push ( this.name ); 
		this.users[ socket.nick ] = {};
		this.users[ socket.nick ]['mode'] = mode;
		this.users[ socket.nick ]['socket'] = socket;

	};
	this.removeUser = function (user){
		global = Array.prototype.GLOBAL;
		if(!this.users[user] )
			return false; // usuario não encontrado
		else {
			delete this.users[user];
			global = Array.prototype.GLOBAL;
			rt = global.USERS[user].channels.indexOf( this.name );;
			global.USERS[user].channels.splice( rt , 1);
		}
		return true; // removido com sucesso
	};
	this.setKey  = function (key){
		this.key = key;
	};
	this.isInvited = function ( user ) {
		ret = this.inviteList.indexOf( user );
		if( ret == -1)
			return false;
		else 
			return true;
			
	};
	this.setMode = function (name, mode){
		if(!this.users[name])
			return false;
		this.users[name].mode = mode;
		return true;
	};
	this.isUser = function(user){
		if(!this.users[user])
			return false;
		else 
			return true;
	};
	this.broadcast = function ( msg , socket ){
		 nusers = []; // nome dos usuarios que receberam a mensagem
		for( name in this.users)
			nusers.push( name );
		r = nusers.indexOf( socket.nick );
		if(r != -1)
			nusers.splice(r, 1); // remove o usuario que ta fazendo a ação
		Array.prototype.usuarios = this.users; // little hack in order to make users visible within forEach
		nusers.forEach( function (usuario){
			if( nusers.indexOf(usuario) != -1 )
				Array.prototype.usuarios [ usuario ].socket.write(msg);
		});
	};	
	////////////////////////////////////////////////////////
	this.broadcastOP = function ( msg , socket ){
		 nusers = []; // nome dos usuarios que receberam a mensagem
		for( name in this.users){
			if( this.users[ name ].mode.toLowerCase() == "o")
				nusers.push( name );
		}
		r = nusers.indexOf( socket.nick );
		if(r != -1)
			nusers.splice(r, 1); // remove o usuario que ta fazendo a ação
		Array.prototype.usuarios = this.users; // little hack in order to make users visible within forEach
		nusers.forEach( function (usuario){
			if( nusers.indexOf(usuario) != -1 )
				Array.prototype.usuarios [ usuario ].socket.write(msg);
		});
	};	
}



