exports.functionname1 = function (args, socket, global) {
//Modelo exemplo
//Ao criar novas funções, manter o modelo e alterar apenas o nome da função
}
exports.functionname2 = function (args, socket, global) {
//Modelo exemplo
//Ao criar novas funções, manter o modelo e alterar apenas o nome da função
//Lembre-se de que para chamar uma variável global, deve-ser o usar a forma global.variable = xxx, envez de variable = x
//Ex: lista de nicks: usar global.nicks[socket.name] envez de nicks[socket.name] ou haverão erros
}
