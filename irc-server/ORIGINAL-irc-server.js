// Load the TCP Library
net = require('net');

// Keep track of the chat clients
var clients = [];

// Start a TCP Server
net.createServer(function (socket) {

  // Identify this client
  socket.name = socket.remoteAddress + ":" + socket.remotePort 

  // Put this new client in the list
  clients.push(socket);

  // Send a nice welcome message and announce
  socket.write("Welcome " + socket.name + "\n");
  broadcast(socket.name + " joined the chat\n", socket);

  // Handle incoming messages from clients.
  socket.on('data', function (data) {
    // broadcast(socket.name + "> " + data, socket);
    // analisar a mensagem
    analisar(data);
  });

  // Remove the client from the list when it leaves
  socket.on('end', function () {
    clients.splice(clients.indexOf(socket), 1);
    broadcast(socket.name + " left the chat.\n");
  });
  
  // Send a message to all clients
  function broadcast(message, sender) {
    clients.forEach(function (client) {
      // Don't want to send it to sender
      if (client === sender) return;
      client.write(message);
    });
    // Log it to the server output too
    process.stdout.write(message)
  }

  function analisar(data) {
    // visualizar os dados como são (sem tratamento)
    // console.log(data);
    // visualizar como string
    // console.log(String(data));
    // visualizar os caracteres especiais enviados
    // console.log(JSON.stringify(String(data)));
    // o método trim remove os caracteres especiais do final da string
    var mensagem = String(data).trim();
    // o método split quebra a mensagem em partes separadas p/ espaço em branco
    var args = mensagem.split(" "); 
    if ( args[0] == "NICK" ) nick(args); // se o primeiro argumento for NICK
    else if ( args[0] == "USER") user(args);
    else if ( args[0] == "JOIN") join(args);
    else socket.write("ERRO: comando inexistente\n");
  }
 
  function nick(args) {
    socket.write("OK: comando NICK executado com sucesso\n");
  }

  function user(args) {
    socket.write("OK: comando USER executado com sucesso\n");
  }

  function join(args) {
    socket.write("OK: comando JOIN executado com sucesso\n");
  }
}).listen(6667);

// Put a friendly message on the terminal of the server.
console.log("Chat server running at port 6667\n");
