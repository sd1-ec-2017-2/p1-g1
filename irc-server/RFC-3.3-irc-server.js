exports.privmsg = function (args, socket, global) {
	args.shift();	//gets rid of the PRIVMSG keyword
	let argsjoin = args.join(' ');
	let argscolon = argsjoin.split(':');
	if(argscolon.length != 2){	//verifies if the arguments are appropriate PRIVMSG <target> : <message>
		socket.write('ERRO: argumentos inválidos\n');	//norecipient+notextsent
		return;
	} else if(!argscolon[1]){	//checks if text has been provided
		socket.write('ERRO: mensagem não fornecida\n');
		return;
	} else if(argscolon[0].charAt(0) === '#'
			|| argscolon[0].charAt(0) === '$'
			|| argscolon[0].charAt(0) === '!'
			|| argscolon[0].charAt(0) === '+'){	//checks if the target is a channel
		var ischannel = true;
	} else if(argscolon[0]==='$*' || argscolon[0]==='#*'){	//checks if it's a broadcast to all channels
		if(!socket.mode.contains('o') && !socket.mode.contains('0')){	//checks if the sender has the operator privileges required for a broadcast command
			socket.write('ERRO: a operação broadcast só é disponível para operadores\n');
			return;
			}
		var broadcast = true;
		ischannel = false;
		}
	var targetexists = false;
	if(ischannel){	//checks if the channel exists
		for(channel in global.channels){
			if(argscolon[0].trim() === global.channels[channel].name){
				targetexists = true;
				if(!global.channels[channel].isUser(socket.nick)){	//checks if you belong to that channel
					socket.write('ERRO: você deve ser membro deste canal para enviar mensagens nele\n');
					return;
				}
				let msg = global.hms()+'['+channel+'] '+'<'+socket.nick+'>'+argscolon[1];
				global.channels[channel].broadcast(msg+'\n', socket);
				socket.write(msg+'\n');
				return;
			}
		}
		socket.write('ERRO: canal inexistente\n');
		return;
		
	} else if(broadcast){	//checks if it's a broadcast to all users
		targetexists = true;
		global.clients.forEach(function(client){
			client.write(global.hms()+'B <'+socket.nick+'> '+argscolon[1]);
		});
		socket.write(global.hms()+'B <'+socket.nick+'> '+argscolon[1]);
		return;
	} else 	//checks if the nick exists
	    global.clients.forEach(function (client) {	//verifies if the nick exists, in case a simple nick wasprovided
			if(client.nick === argscolon[0].trim()){
				targetexists = true;
				if(client.mode.contains('a')){
					client.write(global.hms()+'WHILE AWAY <'+socket.nick+'> '+argscolon[1]+'\n');
					socket.write(global.hms()+'RPL_AWAY <'+client.nick+'> '+client.awaymsg+'\n');
					return;
				}
				client.write(global.hms()+'<'+socket.nick+'> '+argscolon[1]+'\n');
				socket.write(global.hms()+'<'+socket.nick+'> '+argscolon[1]+'\n');
				return;
			}
		});
	if(!targetexists){
		socket.write('ERRO: receptor inexistente\n');
		return;
	}
}

exports.notice = function (args, socket, global) {
	args.shift();	//gets rid of the PRIVMSG keyword
	let argsjoin = args.join(' ');
	let argscolon = argsjoin.split(':');
	if(argscolon.length != 2){	//verifies if the arguments are appropriate PRIVMSG <target> : <message>
		//: argumentos inválidos\n');	//norecipient+notextsent
		return;
	} else if(!argscolon[1]){	//checks if text has been provided
		//: mensagem não fornecida\n');
		return;
	} else if(argscolon[0].charAt(0) === '#'
			|| argscolon[0].charAt(0) === '$'
			|| argscolon[0].charAt(0) === '!'
			|| argscolon[0].charAt(0) === '+'){	//checks if the target is a channel
		var ischannel = true;
	} else if(argscolon[0]==='$*' || argscolon[0]==='#*'){	//checks if it's a broadcast to all channels
		if(!socket.mode.contains('o') && !socket.mode.contains('0')){	//checks if the sender has the operator privileges required for a broadcast command
			//: a operação broadcast só é disponível para operadores\n');
			return;
			}
		var broadcast = true;
		ischannel = false;
		}
	var targetexists = false;
	if(ischannel){	//checks if the channel exists
		for(channel in global.channels){
			if(argscolon[0].trim() === global.channels[channel].name){
				targetexists = true;
				if(!global.channels[channel].isUser(socket.nick)){	//checks if you belong to that channel
					//: você deve ser membro deste canal para enviar mensagens nele\n');
					return;
				}
				let msg = global.hms()+'['+channel+'] (notice) '+'<'+socket.nick+'>'+argscolon[1];
				global.channels[channel].broadcast(msg+'\n', socket);
				socket.write(msg+'\n');
				return;
			}
		}
		//: canal inexistente\n');
		return;
		
	} else if(broadcast){	//checks if it's a broadcast to all users
		targetexists = true;
		global.clients.forEach(function(client){
			client.write(global.hms()+'B (notice)<'+socket.nick+'> '+argscolon[1]);
		});
		socket.write(global.hms()+'B (notice)<'+socket.nick+'> '+argscolon[1]);
		return;
	} else 	//checks if the nick exists
	    global.clients.forEach(function (client) {	//verifies if the nick exists, in case a simple nick wasprovided
			if(client.nick === argscolon[0].trim()){
				targetexists = true;
				if(client.mode.contains('a')){
					client.write(global.hms()+'WHILE AWAY (notice) <'+socket.nick+'> '+argscolon[1]+'\n');
					socket.write(global.hms()+'RPL_AWAY (notice) <'+client.nick+'> '+client.awaymsg+'\n');
					return;
				}
				client.write(global.hms()+' (notice) <'+socket.nick+'> '+argscolon[1]+'\n');
				socket.write(global.hms()+' (notice) <'+socket.nick+'> '+argscolon[1]+'\n');
				return;
			}
		});
	if(!targetexists){
		//: receptor inexistente\n');
		return;
	}
}
