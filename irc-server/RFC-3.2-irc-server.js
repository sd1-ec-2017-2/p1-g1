exports.join = function (args, socket, global) {
//Modelo exemplo
	args.shift(); // get rid of 'join' 
 	if( args.length == 0 || args.length > 2){ // check if user has passed the appropriate amount of arguments
		socket.write("ERRO: Número de argumentos inválido.\n");
	}
		if( args[0] == '0'){ // user wants to leave all channels
		for( name in global.channels ){
				obj = global.channels [ name ] ;
				if( obj.removeUser( socket.nick ) == true){
					socket.write("Você foi removido do canal \""+name+" com sucesso.\n");
					obj.broadcast("["+name+"] o usuario "+socket.nick+" saiu do canal.\n",socket);
				}
		}
		return ;
	}
	
	canais = [];
	keys = [];
	
		if( args[0].indexOf(',') == -1)
			canais.push(args[0]);
		else
			canais = canais.concat( args[0].split(',') );
	if( args.length == 2){ // in case keys are provided
		if( args[1].indexOf(',') == -1)
			keys.push(args[1]);
		else
			keys = keys.concat( args[1].split(',') );
	}
	canais.forEach(function ( cnome ){ // chech whether the channel names are valid or not
		c = new Channel();
		if( c.isValidName( cnome ) == false){
			socket.write("ERRO: Canal com nome inválido: "+cnome+"\n");
			return ;
		}	
	});
	
/////////////////////////////////////////////	
/////////////////////////////////////////////
	i = 0;
	canais.forEach(function (cnome) {
		senha = "";
		if( keys[i]) 
			senha = keys[i];
		if(!global.channels[ cnome ]){ // channel doesn't exist
			canal = new Channel();
			canal.setName( cnome );
			canal.addUser( socket, 'O' ); //
			global.channels[cnome] = canal;
			socket.write("Canal \""+cnome+"\" foi criado com sucesso.\n");
		}
		else { // channel exists
			obj = global.channels[ cnome ];
			if( obj.inviteOnly == true && obj.isInvited( socket.nick ) == false )
				socket.write("ERRO: Canal \""+cnome+"\" é INVITE_ONLY e você não está na lista de convidados.\n");
							
			else {
				if( senha != obj.key){
					socket.write("ERRO: A senha do canal \""+cnome+"\" está incorreta.\n"); 
				}
				else{
					if(obj.isUser( socket.nick ) == true) { socket.write("Você já está no canal \""+cnome+"\".\n"); 
}
					else{ obj.addUser( socket, ""); socket.write("Adicionado ao canal \""+cnome+"\" com sucesso.\n"); 
						obj.broadcast("["+cnome+"] O usuario "+socket.nick+" foi adicinado ao canal.\n",socket);

						} 
					
				}
				
			}
		}
		i++;
	});
	

	
	
};
exports.part = function(args,socket,global){
		args.shift(); // "PART" goes away
		if( args.length == 0) {
				socket.write("ERRO: Número incorreto de Parâmetro.\n");
				return ;
		}
		if(args[1]){
			if(args[1][0] != ":"){
			socket.write("ERRO: Parâmetros inválidos.\n");
			return;
			}
		}
		comment = "";
		canais = [];
		if( args.length == 1)
			comment = "";
		else{
			temp = args;
			temp.shift(); // get rid of the first argumentos
			msg = temp.join("");
			msg = msg.replace(":","");
			comment = "Comentário do usuário: "+msg+"\n";;
		}
		if( args[0].indexOf(',') != -1 )
			canais = canais.concat( args[0].split(',') );
			
		else 
			canais.push(args[0]);
		canais.forEach( function (canal) {
			if(!global.channels[canal])
					socket.write("Canal \""+canal+"\" não foi encontrado.\n");
			
			else{
				ret = global.channels[canal].removeUser( socket.nick );
				if( ret == true){
					socket.write("Você saiu do canal \""+canal+"\" com sucesso.\n");
					global.channels[canal].broadcast("["+canal+"] O usuário "+socket.nick+" saiu do canal.\n"+comment,socket);
				}
				else
					socket.write("Voce não está no canal \""+canal+"\".\n");
				
			}
			
		});
		
			
};



/*
////////////////////////////////////////////////////////
////////////////// Funções Auxiliares //////////////////
/////////////////////////////////////////////////////////
*/
function Channel(){
		this.name = "";
		this.inviteList = []; 
		this.inviteOnly = false;
		this.topic = "";
	//	this.clients = new Array();
		this.users = {};
		this.key = "";
		
	this.isValidName = function (name){
		if( name.length > 50) return false; 
		if( name[0] == '#') return true;
		if( name[0] == '&') return true;
		if( name[0] == '!') return true;
		if( name[0] == '+') return true;
		return false;
	};
	this.getName = function (){
		return this.name;
	};
	this.setName = function (nome){
		this.name = nome;
	};
	this.addUser = function(user, mode){
		user.channels.push( this.name ); 
		this.users[ user.nick ] = {};
		this.users[ user.nick ]['mode'] = mode;
		this.users[ user.nick ]['socket'] = user;

	};
	this.removeUser = function (user){
		global = Array.prototype.GLOBAL;
		if(!this.users[user] )
			return false; // usuario não encontrado
		else {
			delete this.users[user];
			index = socket.channels.indexOf( this.name );
			user.channels.splice(index, 1 );
		}
		return true; // removido com sucesso
	};
	this.setKeybase  = function (key){
		this.key = key;
	};
	this.isInvited = function ( user ) {
		ret = this.inviteList.indexOf( user );
		if( ret == -1)
			return false;
		else 
			return true;
			
	};
	this.isUser = function(user){
		if(!this.users[user])
			return false;
		else 
			return true;
	};
	this.broadcast = function ( msg , socket ){
		 nusers = []; // nome dos usuarios que receberam a mensagem
		for( name in this.users)
			nusers.push( name );
		r = nusers.indexOf( socket.nick );
		if(r != -1)
			nusers.splice(r, 1); // remove o usuario que ta fazendo a ação
		Array.prototype.usuarios = this.users; // little hack in order to make users visible within forEach
		nusers.forEach( function (usuario){
			if( nusers.indexOf(usuario) != -1 )
				Array.prototype.usuarios [ usuario ].socket.write(msg);
		});
	};	
}



